package test.logic;

import static org.junit.Assert.*;
import model.logic.UberLogic;
import model.logic.TravelTime;

import org.junit.Before;
import org.junit.Test;

public class TestUberLogic {

	private UberLogic<TravelTime> modelo;

	@Before
	public void setUp1() {
		modelo= new UberLogic<TravelTime>();
	}

	@Test
	public void setUp2()
	{
		modelo.load("./test/data/Test.csv");
	}
	// Pruebas empiezan Aqu�

	@Test
	public void testUberLogic() {
		assertTrue(modelo!=null);
		assertEquals(0, modelo.getHeapSize());  // Modelo con 0 elementos presentes.
		assertEquals(0, modelo.getColaSize());	
	}

	@Test
	public void testDarTamano() {
		assertEquals(0, modelo.getHeapSize());  // Modelo con 0 elementos presentes.
		assertEquals(0, modelo.getColaSize());  
		modelo.load("./test/data/Test.csv");
		assertEquals(10, modelo.getHeapSize());  // Modelo con 0 elementos presentes.
		assertEquals(10, modelo.getColaSize());  
	}
}
