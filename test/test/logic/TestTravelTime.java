package test.logic;

import model.logic.TravelTime;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class TestTravelTime {

	private TravelTime uberData;

	@Before
	public void setUp1()
	{
		uberData = new TravelTime(1, 2, 3, 4, 5, 2);	
	}

	@Test
	public void testUberDdata()
	{
		assertNotNull(uberData);
	}
	@Test
	public void testGetSourceid() {
		assertTrue(uberData.getSourceid() == 1);
	}
	@Test
	public void testGetDstid() {
		assertTrue(uberData.getDstid() == 2);
	}
	@Test
	public void testGetTime() {
		assertTrue(uberData.getTime() == 3);
	}
	@Test
	public void testGetMean_travel_time() {
		assertTrue(uberData.getMean_travel_time() == 4);
	}
	@Test
	public void testGetStandard_deviation_travel_time() {
		assertTrue(uberData.getStandard_deviation_travel_time() == 5);
	}
	public void testGetTrimestre()
	{
		assertTrue(uberData.getTrimestre() == 2);
	}
}