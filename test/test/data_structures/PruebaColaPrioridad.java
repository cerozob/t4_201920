package test.data_structures;

import static org.junit.Assert.*;

import java.io.FileReader;

import org.junit.Before;
import org.junit.Test;

import com.opencsv.CSVReader;

import model.data_structures.MaxColaCP;
import model.data_structures.MaxHeapCP;
import model.logic.TravelTime;

public class PruebaColaPrioridad
{
	// TODO EKISDE: "./test/data/testsCola" Y "./test/data/testsHeap"
	private final static String TESTFILE = "./test/data/Test.csv";
	private final static String TESTCOLA = TESTFILE;
	private final static String TESTHEAP = TESTFILE;
	
	private MaxHeapCP<TravelTime> heap;
	private MaxColaCP<TravelTime> cola;
	
	@Before
	public void setup()
	{	
		heap = new MaxHeapCP<TravelTime>(10);
		cola = new MaxColaCP<TravelTime>(10);	
	}
	@Test
	public void testAgregarHeap()
	{
		try {
			cargarHeap();
		}
		catch (Exception e)
		{fail("No debe generar excepción");}
	}
	@Test
	public void testAgregarCola()
	{
		try {cargarCola();}
		catch (Exception e)
		{fail("No debe generar excepción");}
	}
	@Test
	public void testSacarMaxHeap()
	{
		TravelTime travel = new TravelTime(5,6,9,2757.13,467.44,888);
		cargarHeap();
		assertTrue(heap.sacarMax().compareTo(travel) == 0);
	}
	@Test
	public void testSacarMaxCola()
	{
		TravelTime travel = new TravelTime(5,6,9,2757.13,467.44,888);
		cargarCola();
		assertTrue(cola.sacarMax().compareTo(travel) == 0);
	}
	
	private void cargarCola()
	{
		int trimestre = 888;
		int contadorLineas = 0;
		try {
			//Reader que lee el csv linea por linea
			CSVReader reader = new CSVReader(new FileReader(TESTCOLA));
			//retorna un arreglo de arreglos de strings
			for(String[] linea : reader) 
			{
				if(contadorLineas != 0) //cuando es 0 est� leyendo la primera l�nea.
				{	
					//el �ndice del nodo empieza en 1
					cola.agregar(new TravelTime(
							Integer.parseInt(linea[0]),
							Integer.parseInt(linea[1]), 
							Integer.parseInt(linea[2]), 
							Double.parseDouble(linea[3]), 
							Double.parseDouble(linea[4]),trimestre));
				}
				contadorLineas++;
			}
			reader.close();
		} catch (Exception e) 
			{e.printStackTrace();}
	}
	private void cargarHeap()
	{
		int trimestre = 333;
		int contadorLineas = 0;
		try {
			//Reader que lee el csv linea por linea
			CSVReader reader = new CSVReader(new FileReader(TESTHEAP));
			//retorna un arreglo de arreglos de strings
			for(String[] linea : reader) 
			{
				if(contadorLineas != 0) //cuando es 0 est� leyendo la primera l�nea.
				{	
					//el �ndice del nodo empieza en 1
					TravelTime travel = new TravelTime(
							Integer.parseInt(linea[0]),
							Integer.parseInt(linea[1]), 
							Integer.parseInt(linea[2]), 
							Double.parseDouble(linea[3]), 
							Double.parseDouble(linea[4]),trimestre);
					heap.agregar(travel);
				}
				contadorLineas++;
			}
			reader.close();
		} catch (Exception e) 
			{e.printStackTrace();}
	}
}
