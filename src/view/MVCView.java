package view;

import model.logic.UberLogic;

public class MVCView 
{
	/**
	 * Metodo constructor
	 */
	public MVCView()
	{

	}

	public void printMenu()
	{
		System.out.println("1. Cargar Archivos");
		System.out.println("2. Generar muestra en MaxHeapCP");
		System.out.println("3. Generar muestra en MaxColaCP");
		System.out.println("4. Tiempo promedio de agregar datos aleatorios a un MaxHeapCP");
		System.out.println("5. Tiempo promedio de agregar datos aleatorios a un MaxColaCP");
		System.out.println("6. Exit");
	}

	public void printMessage(String mensaje) {

		System.out.println(mensaje);
	}		
}
