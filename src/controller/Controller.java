package controller;

import java.util.Scanner;

import model.logic.UberLogic;
import model.logic.TravelTime;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private UberLogic<TravelTime> modelo;

	/* Instancia de la Vista*/
	private MVCView view;

	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new UberLogic<TravelTime>();
	}

	public void run()   
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		long tiempoPromedioHeap = 0;
		long tiempoPromedioCola = 0;

		while( !fin ){
			view.printMenu();
			int option = lector.nextInt();
			switch(option){
			case 1:
				modelo = new UberLogic<TravelTime>(); 
				tiempoPromedioCola = modelo.loadCola("./data/bogota-cadastral-2018-"+1+"-All-HourlyAggregate.csv") + modelo.loadCola("./data/bogota-cadastral-2018-"+2+"-All-HourlyAggregate.csv");
				tiempoPromedioHeap = modelo.loadHeap("./data/bogota-cadastral-2018-"+1+"-All-HourlyAggregate.csv") + modelo.loadHeap("./data/bogota-cadastral-2018-"+2+"-All-HourlyAggregate.csv");
			case 2: 
				int tam = lector.nextInt();
				modelo.generarMuestraHeap(tam);
				System.out.println("Se generó una muestra de tamano " +tam);
				break;
			case 3:
				int tam2 = lector.nextInt();
				modelo.generarMuestraCola(tam2);
				System.out.println("Se generó una muestra de tamano " +tam2);
				break;
			case 4:
				System.out.println("El tiempo promedio para agregar datos aleatorios a un MaxHeapCP es "+ tiempoPromedioHeap);
				break;
			case 5:
				System.out.println("El tiempo promedio para agregar datos aleatorios a un MaxColaCP es "+ tiempoPromedioCola);
				break;
			case 6:
				lector.close();
				break;

			default: 
				System.out.println("--------- \n Opcion Invalida !! \n---------");
				break;
			}

		}

	}	
}
