package model.logic;

public class TravelTime implements Comparable<TravelTime> {

	private int trimestre;
	//id del punto de partida del viaje
	private int sourceid;

	//id del punto de destino
	private int dstid;

	//tiempo del viaje, dependiente dl tipo de la enumeración
	private int hod;

	//tiempo promedio del viaje
	private double mean_travel_time;

	//desviación estandar el tiempo de viaje
	private double standard_deviation_travel_time;

	public TravelTime(int pSourceid,int pDstid,int pTime,double pMean_travel_time,double pStandard_deviation_travel_time,int pTrimestre)
	{
		sourceid = pSourceid;
		dstid = pDstid;
		hod = pTime;
		mean_travel_time = pMean_travel_time;
		standard_deviation_travel_time = pStandard_deviation_travel_time;
		trimestre = pTrimestre;
	}

	public int getSourceid() {
		return sourceid;
	}

	public int getDstid() {
		return dstid;
	}

	public int getTime() {
		return hod;
	}

	public double getMean_travel_time() {
		return mean_travel_time;
	}

	public double getStandard_deviation_travel_time() {
		return standard_deviation_travel_time;
	}

	public int getTrimestre()
	{
		return trimestre;
	}
	@Override
	public int compareTo(TravelTime comparar) {
		// compara dos objetos de tipo UberData por el tiempo promedio de viaje,
		//retorna -1 si el objeto por parámetro es mayor, 1 en caso contrario y 0 si son iguales.
		if(comparar != null)
		{
			if (mean_travel_time < comparar.getMean_travel_time())
			{
				return -1;
			}
			else if(mean_travel_time > comparar.getMean_travel_time())
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		return 698779;
	}
}