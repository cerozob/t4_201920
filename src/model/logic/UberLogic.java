package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.opencsv.CSVReader;
import model.data_structures.MaxColaCP;
import model.data_structures.MaxHeapCP;

/**
 * Definicion del modelo del mundo
 *
 */
public class UberLogic<T> {

	/**
	 * Atributos del modelo del mundo
	 */
	private MaxHeapCP<TravelTime> heap;
	private MaxColaCP<TravelTime> cola;
	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public UberLogic()
	{
		heap = new MaxHeapCP<TravelTime>(1000);
		cola = new MaxColaCP<TravelTime>(1000);
	}

	/**
	 * Servicio de consulta de numero de elementos presentes en el modelo 
	 * @return numero de elementos presentes en el modelo
	 */
	public int getHeapSize()
	{
		return heap.darNumElementos();
	}

	public int getColaSize()
	{
		return cola.darNumElementos();
	}

	//TODO Metodo de cargar CSV.
	public Long loadCola(String pRutaArchivo)
	{
		int trimestre = 0;
		if(pRutaArchivo.contains("-1-"))
		{
			trimestre = 1;
		}
		else if (pRutaArchivo.contains("-2-"))
		{
			trimestre = 2;
		}
		int contadorLineas = 0;
		Long startTime = System.currentTimeMillis();
		Long finalTime;
		Long sumaTiempos = 0L;
		Long cuentaTiempos = 0L;
		try {
			//Reader que lee el csv linea por linea
			CSVReader reader = new CSVReader(new FileReader(pRutaArchivo));
			//retorna un arreglo de arreglos de strings
			
			for(String[] linea : reader) 
			{
				if(contadorLineas != 0) //cuando es 0 est� leyendo la primera l�nea.
				{	
					//el �ndice del nodo empieza en 1
					TravelTime travel = new TravelTime(
							Integer.parseInt(linea[0]),
							Integer.parseInt(linea[1]), 
							Integer.parseInt(linea[2]), 
							Double.parseDouble(linea[3]), 
							Double.parseDouble(linea[4]),trimestre);
					cola.agregar(travel);
					if((contadorLineas % 200000) == 0)
					{
						finalTime = System.currentTimeMillis();
						sumaTiempos +=(finalTime-startTime);
						startTime = finalTime;
						cuentaTiempos++;
					}
				}
				contadorLineas++;
			}
			reader.close();
		} catch (Exception e) {

			e.printStackTrace();
		}
		Long promedio = (sumaTiempos/cuentaTiempos)/1000;
		return promedio;
	}
	
	public Long loadHeap(String pRutaArchivo)
	{
		int trimestre = 0;
		if(pRutaArchivo.contains("-1-"))
		{
			trimestre = 1;
		}
		else if (pRutaArchivo.contains("-2-"))
		{
			trimestre = 2;
		}
		int contadorLineas = 0;
		Long startTime = System.currentTimeMillis();
		Long finalTime;
		Long sumaTiempos = 0L;
		Long cuentaTiempos = 0L;
		try {
			//Reader que lee el csv linea por linea
			CSVReader reader = new CSVReader(new FileReader(pRutaArchivo));
			//retorna un arreglo de arreglos de strings
			
			for(String[] linea : reader) 
			{
				if(contadorLineas != 0) //cuando es 0 est� leyendo la primera l�nea.
				{	
					//el �ndice del nodo empieza en 1
					TravelTime travel = new TravelTime(
							Integer.parseInt(linea[0]),
							Integer.parseInt(linea[1]), 
							Integer.parseInt(linea[2]), 
							Double.parseDouble(linea[3]), 
							Double.parseDouble(linea[4]),trimestre);
					heap.agregar(travel);
					if((contadorLineas % 200000) == 0)
					{
						finalTime = System.currentTimeMillis();
						sumaTiempos +=(finalTime-startTime);
						startTime = finalTime;
						cuentaTiempos++;
					}
				}
				contadorLineas++;
			}
			reader.close();
		} catch (Exception e) {

			e.printStackTrace();
		}
		Long promedio = (sumaTiempos/cuentaTiempos)/1000;
		return promedio;
	}
	/**
	 * Genera una muestra de N viajes que no se repiten
	 */
	public MaxColaCP<TravelTime> generarMuestraCola(int n)
	{
		MaxColaCP<TravelTime> muestra = new MaxColaCP<TravelTime>(n);
		TravelTime anterior = cola.sacarMax();
		while(muestra.darNumElementos() < n) 
		{
			TravelTime actual = cola.sacarMax();
			if(!anterior.equals(actual)) 
				muestra.agregar(actual);
			anterior = actual;
			actual = cola.sacarMax();
		}
		return muestra;
	}

	public MaxHeapCP<TravelTime> generarMuestraHeap(int n)
	{
		MaxHeapCP<TravelTime> muestra = new MaxHeapCP<TravelTime>(n);
		TravelTime anterior = heap.sacarMax();
		while(muestra.darNumElementos() < n) 
		{
			TravelTime actual = heap.sacarMax();
			if(!anterior.equals(actual)) 
				muestra.agregar(actual);
			anterior = actual;
			actual = cola.sacarMax();
		}
		return muestra;
	}

	public MaxColaCP<TravelTime> crearMaxColaCP (int n,int hInicial, int hFinal)
	{
		MaxColaCP<TravelTime> colaHora = new MaxColaCP<TravelTime>(2);
		TravelTime actual;
		while((actual = cola.sacarMax()) != null && n > 0)
		{
			if(actual.getMean_travel_time() >= hInicial && actual.getMean_travel_time() <= hFinal)
			{
				colaHora.agregar(actual);
			}
			n--;
		}
		return colaHora;
	}

	public MaxHeapCP<TravelTime> crearMaxHeapCP (int n,int hInicial, int hFinal)
	{
		MaxHeapCP<TravelTime> heapHora = new MaxHeapCP<TravelTime>(2);
		TravelTime actual;
		while((actual = heap.sacarMax()) != null && n > 0)
		{
			if(actual.getMean_travel_time() >= hInicial && actual.getMean_travel_time() <= hFinal)
			{
				heapHora.agregar(actual);
			}
			n--;
		}
		return heapHora;
	}
	
	public MaxHeapCP<TravelTime> agregarDatosOrdenadosAscendentementeAUnHeapCPVacio (MaxHeapCP<TravelTime> datos)
	{
		MaxHeapCP<TravelTime> rta = new MaxHeapCP<TravelTime>(datos.darNumElementos());
		long ti = System.currentTimeMillis();
		for (int i = 0; i < datos.darNumElementos();i++)
			rta.agregar(datos.sacarMax());
		long tf = System.currentTimeMillis();
		long tiempo = (tf-ti)/1000;
		System.out.println("Agregar los "+datos.darNumElementos()+ " se demoro " +tiempo+ " segundos");
		return rta;
	}

	public MaxColaCP<TravelTime> agregarDatosOrdenadosAscendentementeAUnaColaCPVacia (MaxColaCP<TravelTime> datos)
	{
		MaxColaCP<TravelTime> rta = new MaxColaCP<TravelTime>(datos.darNumElementos());
		long ti = System.currentTimeMillis();
		for (int i = 0; i < datos.darNumElementos();i++)
			rta.agregar(datos.sacarMax());
		long tf = System.currentTimeMillis();
		long tiempo = (tf-ti)/1000;
		System.out.println("Agregar los "+datos.darNumElementos()+ " se demoro " +tiempo+ " segundos");
		return rta;
	}
	

	public int load(String pRutaArchivo)
	{
		Long promediocola = loadCola(pRutaArchivo);
		Long promedioheap = loadHeap(pRutaArchivo);
		System.out.println("promediocola   "+promediocola);
		System.out.println("promedioheap   "+promedioheap);
		return cola.darNumElementos();
	}

	public MaxColaCP<TravelTime> agregarDatosOrdenadosDescendentementeAUnaColaCPVacia (MaxColaCP<TravelTime> datos)
	{
		MaxColaCP<TravelTime> rta = new MaxColaCP<TravelTime>(datos.darNumElementos());
		TravelTime[] invertida = (TravelTime[])new Comparable[datos.darNumElementos()];
		for (int i = invertida.length; i > 0 ; i--) 
			invertida[i] = datos.sacarMax();
		long ti = System.currentTimeMillis();
		for (int i = 0; i < datos.darNumElementos();i++)
			rta.agregar(invertida[i]);
		long tf = System.currentTimeMillis();
		long tiempo = (tf-ti)/1000;
		System.out.println("Agregar los "+datos.darNumElementos()+ " se demoro " +tiempo+ " segundos");
		return rta;
	}
	
	public MaxHeapCP<TravelTime> agregarDatosOrdenadosDescendentementeAUnHeapCPVacio (MaxHeapCP<TravelTime> datos)
	{
		MaxHeapCP<TravelTime> rta = new MaxHeapCP<TravelTime>(datos.darNumElementos());
		TravelTime[] invertida = (TravelTime[])new Comparable[datos.darNumElementos()];
		for (int i = invertida.length; i > 0 ; i--) 
			invertida[i] = datos.sacarMax();
		long ti = System.currentTimeMillis();
		for (int i = 0; i < datos.darNumElementos();i++)
			rta.agregar(invertida[i]);
		long tf = System.currentTimeMillis();
		long tiempo = (tf-ti)/1000;
		System.out.println("Agregar los "+datos.darNumElementos()+ " se demoro " +tiempo+ " segundos");
		return rta;
	}
	
	public void sacarTodoDeUnaMaxColaCP (MaxColaCP<TravelTime> datos)
	{
		long ti = System.currentTimeMillis();
		for (int i = 0; i < datos.darNumElementos();i++)
			datos.sacarMax();
		long tf = System.currentTimeMillis();
		long tiempo = (tf-ti)/1000;
		System.out.println("Agregar los "+datos.darNumElementos()+ " se demoro " +tiempo+ " segundos");
	}
	public void sacarTodoDeUnaMaxHeapCP (MaxHeapCP<TravelTime> datos)
	{
		long ti = System.currentTimeMillis();
		for (int i = 0; i < datos.darNumElementos();i++)
			datos.sacarMax();
		long tf = System.currentTimeMillis();
		long tiempo = (tf-ti)/1000;
		System.out.println("Agregar los "+datos.darNumElementos()+ " se demoro " +tiempo+ " segundos");
	}
}	

